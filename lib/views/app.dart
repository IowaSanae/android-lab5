import 'package:flutter/material.dart';
import 'plan_creator_screen.dart';

class MyPlanApp extends StatelessWidget {
  const MyPlanApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'MyPlan',
      home: Scaffold(
        body: PlanCreatorScreen(),
      ),
    );
  }

}

